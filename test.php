<?php
date_default_timezone_set("Asia/Ho_Chi_Minh");
$gender = array("2" => "Nam", "1" => "Nữ");
$arr = array(""=>"","MAT"=>"Khoa học máy tính","KDL" => "Khoa học vật liệu");
?>
<html>
    <head>
        <link rel='stylesheet' href='//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
        <title>Document</title>
    </head>

<style>
    body {
        background: #eee !important;	
    }
    .wrapper {	
        margin-top: 80px;
        margin-bottom: 80px;
    }
    .form-signin {
        max-width: 500px;
        padding: 15px 35px 45px;
        margin: 0 auto;
        background-color: #fff;
        border: 1px solid rgba(0,0,0,0.1);
        border-radius: 10px/20px;
        border: 1px solid #000;
    }
    
    .info{
        width: 30%;
        background: #70AD47;
        padding: 5px 5px;
        border: 1px solid #000;
        color: #fff;
        text-align: center;
    }
    .profile{
        width: 50%px;
        padding: 5px 5px;
        margin-top: 5px;
        border-radius: 10px/20px;
    }
    .btn{
        margin-top:10px;
        max-width: 180px;
        margin-right: auto;
        margin-left: auto;
        background: #70AD47;;
    }

    .btn:hover{
        background-color: green;
    }

    .container{
        display: inline-block;
        position:relative;
        cursor:pointer;
        font-size:18px;
        user-select: none;
        padding-left:30px;
        padding-right:40px;
        margin-bottom:10px;
    }
    .container input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }
    .checkmark{
        position:absolute;
        top:0;
        left:0;
        width:20px;
        height:20px;
        background:#eee;
        border-radius:50%;
    }
    .container:hover .checkmark{
        background:#ccc;
    }
    .checkmark:after{
        content:"";
        position:absolute;
        display:none;
    }
    .container .checkmark:after{
        top:50%;
        left:50%;
        width: 15px;
        height: 15px;
        border-radius:50%;
        border: solid 3px white;
        transform:translate(-50%,-50%) rotate(45deg);
    }
    .container input:checked ~ .checkmark{
        background:#2196F3;
    }
    .container input:checked ~ .checkmark:after{
        display:block;
    }

	.data{
		padding-left: 20px; 
		display: inline-block;
	}

	.image-box{
		display: flex;
		flex-direction: row;
		align-items: flex-start;
	}
</style>
   
<body>
<?php
session_start();
?>
<div class = 'wrapper'>
	<div class = 'form-signin'>
		<div>
			<label class = 'info'>Họ và tên<span style="color: red;">*</span></label>
			<div class="data">
				<?php
					echo $_SESSION['fullname'];
				?>
			</div>
			
		</div>

		<div>
			<label class = 'info'>Giới tính<span style="color: red;">*</span></label>
			<div class="data">
				<?php
					echo $gender[$_SESSION['gender']];
				?>
			</div>
		</div>

		<div>
			<label class = 'info'>Phân khoa<span style="color: red;">*</span></label>
			
			<div class="data">
				<?php
					echo $arr[$_SESSION['type']];
				?>
			</div>
		</div>

		<div>
			<label class = 'info'>Ngày sinh<span style="color: red;">*</span></label>
			<div class="data">
				<?php
					echo $_SESSION['date'];
				?>
			</div>
		</div>
		<div>
			<label class = 'info'>Địa chỉ</label>
			<div class="data">
				<?php
						echo $_SESSION['address'];
				?>
			</div>
			
		</div>

		<div>
			<div class="image-box">
				<label class = 'info'>Hình ảnh</label>
				<div class="data ">
					<?php
						$image_name = $_SESSION['image'];
						echo "<img style='width: 150px;' src='$image_name' >";  
					?>
				</div>
			</div>
		</div>
	</div>
</div> 
</body>
</html>